# -*- mode: ruby -*-
# vi: set ft=ruby :
require 'yaml'

# vagrant api version定数
VAGRANTFILE_API_VERSION = "2"
# vagrabt vm provider定数
VAGRANT_VM_PROVIDER = "virtualbox"

# baseディレクトリ
BASE_DIR = File.expand_path(File.dirname(__FILE__))

ENV['ANSIBLE_CONFIG'] = "#{BASE_DIR}/ansible.cfg"

# vagrantディレクトリ
VAGRANT_CORE_DIR = "#{BASE_DIR}/.vagrant"

# ansibleのgroup_varsを読み込み
settings = YAML.load(File.read("#{BASE_DIR}/group_vars/all.yml"))

# インスタンス構築する情報を配列で定義
clusters = settings["vboxs"]

Vagrant.configure(VAGRANTFILE_API_VERSION) do | config |
  config.ssh.insert_key = true
 
  if Vagrant.has_plugin?("vagrant-cachier")
    config.cache.scope = :machine
    config.cache.enable :apt
  end 

  # インスタンス作成
  ansible_raw_ssh_args = []
  ansible_groups = {}
  clusters.each_with_index do | (info), index |
      config.vm.define info["name"] do |cfg| 
           
          cfg.vm.provider :virtualbox do |vb, override|
    
              override.hostmanager.aliases = info["fqdn"]
              override.vm.box = info["box"]
              override.vm.network :private_network, ip: info["ipaddr"]
              # override.vm.network :forwarded_port, guest: 22, host: info["ssh_port"]
              override.vm.hostname = info["name"]
              override.hostsupdater.aliases = [info["fqdn"]]
              override.ssh.insert_key = false
              override.ssh.forward_agent = true
              vb.name = info["name"] + "-16348"
              vb.customize ["modifyvm", :id, "--memory", info['memory'], "--cpus", info["cpus"], "--hwvirtex", "on" ]
          end 

          ansible_groups.store(info['group'], [info["name"]])

          # 最後のインスタンス構築後プロビジョニング実行
          if index == clusters.size - 1
              cfg.vm.provision :ansible do | ansible |
                  ansible.playbook = "playbook.yml"
                  ansible.inventory_path = "hosts"
                  ansible.verbose = "-vvvv"
                  ansible.limit = "all"
                  ansible.groups = ansible_groups
              end
          end
      end
  end
end
